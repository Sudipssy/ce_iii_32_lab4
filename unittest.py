import unittest
from greedy import ActSelection,ActSelectionRecur,insertionSort


class ActivityTest(unittest.TestCase):
	def test_selection(self):
		events=[[1, 4], [3, 5], [0, 6], [5, 7], [5, 9], [3, 9], [6, 10], [8, 11], [8, 12], [2, 14], [12, 16]]
		index=[0,3,7,10]
		self.assertListEqual(ActSelection(events,0),index)

	def  test_selection_recursive(self):
		events=[[1, 4], [3, 5], [0, 6], [5, 7], [5, 9], [3, 9], [6, 10], [8, 11], [8, 12], [2, 14], [12, 16]]
		index=[0,3,7,10]
		self.assertListEqual(ActSelectionRecur(events,0,0,len(events)),index)

if  __name__=="__main__":
	unittest.main()